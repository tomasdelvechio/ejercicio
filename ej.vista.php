<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <nav>
        <a href="/">Volver</a>
    </nav>
    <table>
        <thead>
            <tr>
                <?php foreach ($productos[0] as $col_name => $col_value) : ?>
                    <th><?= ucfirst($col_name) ?></th>
                <?php endforeach ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($productos as $producto) : ?>
                <tr>
                    <?php foreach ($producto as $col_value) : ?>
                        <td><?= $col_value ?></td>
                    <?php endforeach ?>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</body>
</html>
