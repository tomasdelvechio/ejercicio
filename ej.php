<?php
include 'vendor/autoload.php';
include 'query.php';

if  (isset($_GET["productos"])) {
    $smarty = new Smarty;
    $resultado = ejecutarQuery('SELECT * FROM productos');
    $productos = pg_fetch_all($resultado);
    $smarty->assign('cabecera', array_keys($productos[0]));
    $smarty->assign('productos', $productos);
    $smarty->display('vista.productos.tpl');
}

if (isset($_POST["insertar"]))
{
    $id_producto = $_POST["id_producto"];
    $descripcion = $_POST["descripcion"];
    $precio_base = $_POST["precio_base"];
    $cantidad = $_POST["cantidad"];
    $descripcion = "'".$descripcion."'";
    $query = 'INSERT INTO "productos"(
        "id_producto", "descripcion", "precio_base", "cantidad")
        VALUES ('.$id_producto.','.$descripcion.','.$precio_base.','.$cantidad.')';
    $resultado = ejecutarQuery($query);
    $smarty = new Smarty;
    $smarty->assign('resultado', $resultado);
    $smarty->display('vista.abm.tpl');
}

if (isset($_POST["actualizar"]))
{
    $id_producto = $_POST["id_producto"];
    $descripcion = $_POST["descripcion"];
    $precio_base = $_POST["precio_base"];
    $cantidad = $_POST["cantidad"];
    $descripcion = "'".$descripcion."'";

    $producto = ejecutarQuery('SELECT *
    FROM "productos" WHERE "id_producto"='.$id_producto);


    if($producto!=""){
        $row = pg_fetch_row($producto);

        if($precio_base=="")
        {
            $precio_base = row[2];
        }

        if($cantidad=="")
        {
            $cantidad = row[3];
        }
        $query = 'UPDATE "productos"
        SET  "descripcion"=coalesce("descripcion"||'.$descripcion.'), "precio_base"='.$precio_base.', "cantidad"='.$cantidad.'
        WHERE '.$id_producto.'="id_producto"';
        if (ejecutarQuery($query))
        {
            $resultado = ("Se modifico el producto");
        }
    }else $resultado = "ID del PRODUCTO INGRESADO NO EXISTE";

    $smarty = new Smarty;
    $smarty->assign('resultado', $resultado);
    $smarty->display('vista.abm.tpl');
}

if (isset($_POST["eliminar"]))
{
    $id_producto = $_POST["id_producto"];

    $producto = ejecutarQuery('SELECT "id_producto", "descripcion", "precio_base", "cantidad"
    FROM "productos" WHERE "id_producto"='.$id_producto);

    if($producto!='')
    {
        $query = 'DELETE FROM "productos"
        WHERE '.$id_producto.'="id_producto"';
        if (ejecutarQuery($query))
        {
            $resultado = ("Se elimino el producto");
        }
    }
    else
    {
      $resultado = "ID del PRODUCTO INGRESADO NO EXISTE";
    }

    $smarty = new Smarty;
    $smarty->assign('resultado', $resultado);
    $smarty->display('vista.abm.tpl');

}
