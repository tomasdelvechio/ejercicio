<?php

    function connection($connection)
    {
        $dbconn = pg_connect($connection) or die('No se ha podido conectar: ' . pg_last_error());
        return $dbconn;
    }

    function close()
    {
        pg_close($dbconn);
    }

    function query($query)
    {
        $result = pg_query($query);
        $error = pg_last_error();
        if($error<>'')
        {
          $result = 'La consulta fallo: '. $error;
        }
        return $result;
    }

    function ejecutarQuery($query) {
        $dbconn = connection("host=localhost dbname=nuevadb user=facu password=1234");
        $result = query($query);
        pg_close($dbconn);
        return $result;
    }
