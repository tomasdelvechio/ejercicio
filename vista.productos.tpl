<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                {foreach $cabecera as $nombre}
                    <th>{$nombre|capitalize|replace:'_':' '}</th>
                {/foreach}
            </tr>
        </thead>
        <tbody>
            {foreach $productos as $producto}
              <tr>
                {foreach $producto as $valor}
                    <td>{$valor}</td>
                {/foreach}
                <td><a href="formularioEliminar.html">eliminar</a></td>
                <td><a href="formularioActualizacion.html">modificar</a></td>
              </tr>
            {/foreach}
        </tbody>
    </table>
    <br />
    <nav>
        <a href="/">Volver</a>
    </nav>
</body>
</html>
