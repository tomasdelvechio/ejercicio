# Ejercicio PIR

## Guia de instalacion

1. Clonar el repositorio en un directorio local e ingresar

```bash
git clone <url> ~/workspace/ejercicio
cd ~/workspace/ejercicio
```

2. Crear la base de datos (debian y ubuntu)

```bash
createdb nuevadb -U postgres
```

3. Aplicar el modelo a la base

```bash
psql nuevadb -U postgres -f sql/esquema.sql
```

4. Instalar dependencias (debian y ubuntu)

```bash
sudo apt install php7.4-pgsql
composer install # Requiere composer instalado globalmente
```

5. Iniciar servidor

```bash
php -S localhost:8000
```

Y dirigirse a http://localhost:8000
